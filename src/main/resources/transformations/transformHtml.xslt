<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<xsl:apply-templates select="root" />
		</html>
	</xsl:template>
	<xsl:template match="root">
		<head>
			<style type="text/css">
				.segment-container{
				border : 1px black solid;
				width :
				60px;
				margin-right: 10px;
				}
				.segment-container:hover{
					background-color:#ffffcc;
				}
				.segment-title{
				padding: 1px 8px 1px 8px;
				}
				.vertical-line {
				border-right: 1px black solid;
				width: 20px;
				height:
				20px;
				}
				td, tr, table {
				margin: 0;
				padding: 0;
				border: 0;
				font-size: 100%;
				font: inherit;
				vertical-align: text-top;
				}
				.top-border {
				border-top: 1px
				black solid
				}
				.segment-name {
				color : blue;
				text-decoration : underline;
				}
				.group-name {
				color : orange;
				text-decoration : underline;
				}
				.fixedPathViewer {
				position: fixed;
				bottom: 20px;
				right: 20px;
				}
				.pathViewerBox {
				width : 500px;
				border : 2px red solid;
				}
			</style>
		</head>

		<body>
			<table class="top-border">
				<tr>
					<xsl:apply-templates select="node()">
						<xsl:with-param name="parentName"></xsl:with-param>
					</xsl:apply-templates>
				</tr>
			</table>
			<div class="fixedPathViewer">
				<input type="text" class="pathViewerBox" id="pathViewer" />
			</div>
			<script type="text/javascript">
				function showPath(jsObject) {
				document.getElementById('pathViewer').value =
				jsObject.getAttribute("fullPath");
				document.getElementById('pathViewer').select();
				}
			</script>
		</body>
	</xsl:template>
	<xsl:template match="segment">
		<xsl:param name="separator" select="'true'" />
		<xsl:param name="parentName" />
		<xsl:variable name="fullPath">
			<!-- Make a string in the format: SG1.SG2.CTA . Concat parent name + '.' 
				+ node name and remove starting '.' -->
			<xsl:value-of select="replace(concat($parentName, '.', .) , '^\.+', '')" />
		</xsl:variable>
		<td>
			<xsl:if test="$separator = 'true'">
				<div class="vertical-line" />
			</xsl:if>
			<table class="segment-container" onclick="showPath(this)">
				<xsl:attribute name="fullPath"><xsl:value-of select="$fullPath" /></xsl:attribute>
				<tr>
					<td colspan="2" class="segment-title">
						<span class="segment-name">
							<xsl:value-of select="." />
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<xsl:value-of select="./@type" />
					</td>
					<td>
						<xsl:value-of select="./@occurence" />
					</td>
				</tr>
			</table>
		</td>
	</xsl:template>
	<xsl:template match="group">
		<xsl:param name="parentName" />

		<xsl:variable name="fullPath">
			<!-- Make a string in the format: SG1.SG2.CTA . Concat parent name + '.' 
				+ node name and remove starting '.' -->
			<xsl:value-of
				select="replace(concat($parentName, '.', ./@name) , '^\.+', '')" />
		</xsl:variable>
		<td>
			<table cellspacing="0">
				<tr>
					<td>
						<div class="vertical-line">
						</div>
						<table class="segment-container" onclick="showPath(this)">
							<xsl:attribute name="fullPath"><xsl:value-of
								select="$fullPath" /></xsl:attribute>
							<tr>
								<td colspan="2" class="segment-title">
									<span class="group-name">
										<xsl:value-of select="./@name" />
									</span>
								</td>
							</tr>
							<tr>
								<td><xsl:value-of select="./@type" /></td>
								<td>
									<xsl:value-of select="./@c" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<xsl:apply-templates select="node()[position() = 1]">
						<xsl:with-param name="separator">
							false
						</xsl:with-param>
						<xsl:with-param name="parentName">
							<xsl:value-of select="$fullPath" />
						</xsl:with-param>
					</xsl:apply-templates>
				</tr>
				<tr>
					<xsl:for-each select="node()[position()>1]">
						<xsl:apply-templates select=".">
							<xsl:with-param name="parentName">
								<xsl:value-of select="$fullPath" />
							</xsl:with-param>
						</xsl:apply-templates>
					</xsl:for-each>
				</tr>
			</table>
		</td>
	</xsl:template>
</xsl:stylesheet>
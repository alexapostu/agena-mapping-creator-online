>>GET_ALL_MAPPINGS
select * from mapping

>>INSERT_MAPPING
insert into mapping(name, xlsName, xlsPath, schemaURL, inputDictionaryName) values(?, ?, ?, ?, ?)

>>GET_MAPPING_BY_ID
select * from mapping where id = ?

>>DELETE_MAPPING_BY_ID
delete from mapping where id = ?
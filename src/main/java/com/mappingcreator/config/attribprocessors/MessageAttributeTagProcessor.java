package com.mappingcreator.config.attribprocessors;

import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.StandardExpressions;
import org.thymeleaf.templatemode.TemplateMode;
import org.unbescape.html.HtmlEscape;

import com.mappingcreator.core.util.validation.ValidationMessage;

public class MessageAttributeTagProcessor extends AbstractAttributeTagProcessor {

	private static final String ATTR_NAME = "text";
	private static final int PRECEDENCE = 10000;

	public MessageAttributeTagProcessor(final String dialectPrefix) {
		super(TemplateMode.HTML, // This processor will apply only to HTML mode
				dialectPrefix, // Prefix to be applied to name for matching
				null, // No tag name: match any tag name
				false, // No prefix to be applied to tag name
				ATTR_NAME, // Name of the attribute that will be matched
				true, // Apply dialect prefix to attribute name
				PRECEDENCE, // Precedence (inside dialect's precedence)
				true); // Remove the matched attribute afterwards
	}

	protected void doProcess(final ITemplateContext context, final IProcessableElementTag tag,
			final AttributeName attributeName, final String attributeValue,
			final IElementTagStructureHandler structureHandler) {

		IEngineConfiguration configuration = context.getConfiguration();
		IStandardExpression expression = StandardExpressions.getExpressionParser(configuration).parseExpression(context,
				attributeValue);
		ValidationMessage message = (ValidationMessage) expression.execute(context);

		final String i18nMessage = context.getMessage(MessageAttributeTagProcessor.class, message.getCode(),
				message.getParams(), true);
		structureHandler.setBody(HtmlEscape.escapeHtml5(i18nMessage), true);

	}

}
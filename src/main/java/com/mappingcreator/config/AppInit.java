package com.mappingcreator.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mappingcreator.core.mapping.formula.FormulaManager;
import com.mappingcreator.core.menu.MenuDescriptor;
import com.mappingcreator.core.util.GlobalConfig;
import com.mappingcreator.core.util.dbconnector.GenericDAO;
import com.mappingcreator.core.util.dbconnector.SQLiteConnectionCreator;
import com.mappingcreator.core.util.queryprocessing.QueryManager;

public class AppInit {

    private static boolean      appConfigured = false;
    private static final Logger LOGGER        = LoggerFactory.getLogger(AppInit.class);

    public static synchronized void init() {
        if (appConfigured) {
            return;
        }
        // Load app settings into GlobalConfig
        GlobalConfig.initGlobalConfig();
        // Fill thread pool
        initDBPool();
        // Menu items init
        MenuDescriptor.init();
        // Init query manager
        QueryManager.init();
        // Init formula manager
        FormulaManager.init();

        // App configured successfully
        LOGGER.error("** mappingcreator APPLICATION STARTED **");

        appConfigured = true;
    }

    private static void initDBPool() {
        int nrOfCons = Integer.valueOf(GlobalConfig.get("conPoolSize"));
        for (int i = 0; i < nrOfCons; i++) {
            GenericDAO.registerConnection(SQLiteConnectionCreator.getConnection());
        }
    }
}

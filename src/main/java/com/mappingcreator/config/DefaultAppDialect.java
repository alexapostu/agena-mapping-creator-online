package com.mappingcreator.config;

import java.util.HashSet;
import java.util.Set;

import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;

import com.mappingcreator.config.attribprocessors.MessageAttributeTagProcessor;

public class DefaultAppDialect extends AbstractProcessorDialect {
	public DefaultAppDialect() {
		super("Default Application dialect", "fm", 1000);
	}

	@Override
	public Set<IProcessor> getProcessors(final String dialectPrefix) {
		final Set<IProcessor> processors = new HashSet<IProcessor>();
		processors.add(new MessageAttributeTagProcessor(dialectPrefix));
		return processors;
	}

}

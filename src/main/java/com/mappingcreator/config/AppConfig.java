package com.mappingcreator.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ITemplateResolver;

@Configuration
@EnableWebMvc
public class AppConfig extends WebMvcConfigurerAdapter implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Bean
    public ITemplateResolver templateResolver() {
        SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
        resolver.setApplicationContext(applicationContext);
        resolver.setPrefix("/");
        resolver.setSuffix(".html");

        resolver.setTemplateMode(TemplateMode.HTML);
        return resolver;
    }
    
    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver 
                = new CommonsMultipartResolver();
        return multipartResolver;
    }

    /*
     * @Bean public SpringTemplateEngine templateEngine() { Set<IDialect>
     * dialects = new HashSet<>(); dialects.add(new LayoutDialect());
     * 
     * SpringTemplateEngine templateEngine = new SpringTemplateEngine();
     * templateEngine.setEnableSpringELCompiler(true);
     * templateEngine.setTemplateResolver(templateResolver());
     * templateEngine.setAdditionalDialects(dialects);
     * templateEngine.setTemplateResolver(templateResolver());
     * templateEngine.addDialect(new DefaultAppDialect());
     * 
     * return templateEngine; }
     */
    @Bean
    public TemplateEngine templateEngine() {
        SpringTemplateEngine engine = new SpringTemplateEngine();

        engine.addDialect(new DefaultAppDialect());
        engine.setTemplateResolver(templateResolver());

        return engine;
    }

    @Bean
    public ThymeleafViewResolver viewResolver() {
        ThymeleafViewResolver resolver = new ThymeleafViewResolver();
        resolver.setTemplateEngine(templateEngine());
        resolver.setOrder(1);
        resolver.setCharacterEncoding("UTF-8");
        resolver.setViewNames(new String[] { "*", "js/*", "templates/*", "fragments/*" });
        return resolver;
    }/*
     
     @Bean
     public MessageSource messageSource() {
     final ResourceBundleMessageSource messageSource;
     
     messageSource = new ResourceBundleMessageSource();
     messageSource.setDefaultEncoding("UTF-8");
     messageSource.setBasename("classpath:i18n/dictionary/dictionary");//,				"classpath:i18n/validation/validation");
     
     return messageSource;
     }*/

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
}

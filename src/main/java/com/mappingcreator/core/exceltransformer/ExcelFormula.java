package com.mappingcreator.core.exceltransformer;

public class ExcelFormula {

    private int    column;
    private String sheetName;
    private String description;
    private String formula;

    public ExcelFormula(int column, String sheetName, String description, String formula) {
        this.column = column;
        this.sheetName = sheetName;
        this.description = description;
        this.formula = formula;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }
}

package com.mappingcreator.core.exceltransformer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;

public class ExcelUtil {

    private static final String INTEGER_PATTERN     = "\\d+";
    private static final String CELL_FORMAT_PATTERN = "([$]?[A-Z]+[0-9]+)";

    public static void addStringValueToCell(String value, HSSFRow row, int column) {
        addValueToCell(value, row, column, CellType.STRING);
    }

    public static void addValueToCell(String value, HSSFRow row, int column, CellType cellType) {

        if (row.getCell(column) == null) {
            row.createCell(column);
        }
        HSSFCell cell = row.getCell(column);
        // If there was any data here, setting it blank should clear it
        cell.setCellType(CellType.BLANK);
        cell.setCellType(cellType);
        switch (cellType) {
            case FORMULA:
                cell.setCellFormula(value);
                break;
            default:
            case STRING:
                cell.setCellValue(value);
                break;
        }

    }

    public static String generateDraggedFormula(String baseFormula) {
        String newFormula = baseFormula;
        Pattern cellRecognitionPattern = Pattern.compile(CELL_FORMAT_PATTERN);
        Matcher m = cellRecognitionPattern.matcher(baseFormula);

        // In the process of replacing strings in the formula, the formula length may change 
        // (replacing string could be bigger or smaller than original)
        // As a result, the start and end positions of the matched patterns will also change
        // This variable keeps track of string length variation between the newFormula and the original
        int lengthVariation = newFormula.length() - baseFormula.length();
        while (m.find()) {
            String cell = m.group(1);
            String newCell = computeDraggedCellPosition(cell);
            newFormula = newFormula.substring(0, m.start() + lengthVariation) + newCell
                    + newFormula.substring(m.end() + lengthVariation);
            lengthVariation = newFormula.length() - baseFormula.length();
        }
        return newFormula;
    }

    private static String computeDraggedCellPosition(String cellIdentifier) {
        String newCellIdentifier = cellIdentifier;
        //if (cellIdentifier.matches(VARIABLE_ROW_PATTERN)) {
        Matcher matcher = Pattern.compile(INTEGER_PATTERN).matcher(cellIdentifier);
        matcher.find();
        int rowNr = Integer.valueOf(matcher.group());
        newCellIdentifier = newCellIdentifier.replace(String.valueOf(rowNr), String.valueOf(rowNr + 1));
        //}
        return newCellIdentifier;
    }

    public static HSSFRow getOrCreateRowBySheetAndIndex(HSSFSheet sheet, int index) {
        if (sheet.getRow(index) == null) {
            sheet.createRow(index);
        }
        HSSFRow formulaRow = sheet.getRow(index);
        return formulaRow;
    }
}

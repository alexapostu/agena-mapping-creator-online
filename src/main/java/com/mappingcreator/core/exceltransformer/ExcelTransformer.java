package com.mappingcreator.core.exceltransformer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mappingcreator.core.mapping.MappingDescriptor;
import com.mappingcreator.core.mapping.formula.FormulaManager;
import com.mappingcreator.core.util.validation.ValidationMessage;

public class ExcelTransformer {
    private static final Logger LOGGER                   = LoggerFactory.getLogger(ExcelTransformer.class);
    private static final int    SEGMENT_IDENTIFIER_VALUE = 0;

    private static final int    LINKS_SHEET_NUMBER       = 1;

    public static List<ValidationMessage> generateNewXLS(String xlsFilePath, MappingDescriptor md) {
        List<ValidationMessage> errors = new ArrayList<ValidationMessage>();
        try (InputStream fis = new FileInputStream(new File(xlsFilePath));
                HSSFWorkbook workbook = new HSSFWorkbook(fis)) {

            renameSheets(workbook);
            ExcelASCIISheetTransformer.addASCIISheetGroupNames(md, errors, workbook);
            ExcelEdifactSheetTransformer.addEdifactSheetGroupNames(md, errors, workbook);
            addFormulasForAllSheets(errors, workbook);
            //HSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);

            try (FileOutputStream fileOut = new FileOutputStream(xlsFilePath)) {
                workbook.write(fileOut);
            }

        } catch (FileNotFoundException fnfe) {
            LOGGER.error("File " + xlsFilePath + " was not found!", fnfe);
        } catch (IOException ioexc) {
            LOGGER.error("Unable to close stream for file " + xlsFilePath, ioexc);
        }
        return errors;
    }

    private static void addFormulasForAllSheets(List<ValidationMessage> errors, HSSFWorkbook workbook) {
        FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
        for (String sheetName : FormulaManager.getFormulaSheetNames()) {
            HSSFSheet sheet = workbook.getSheet(sheetName);
            if (sheet == null) {
                errors.add(new ValidationMessage("validation.no-sheet-for-name", new String[] { sheetName }));
                return;
            }
            HSSFRow initialFormulaRow = ExcelUtil.getOrCreateRowBySheetAndIndex(sheet, 1);
            for (ExcelFormula formula : FormulaManager.getFormulasBySheet(sheetName)) {
                ExcelUtil.addValueToCell(formula.getFormula(), initialFormulaRow, formula.getColumn(),
                        CellType.FORMULA);

                evaluator.evaluateFormulaCellEnum(initialFormulaRow.getCell(formula.getColumn()));
            }

            for (int i = 2; i < sheet.getLastRowNum(); i++) {
                HSSFRow row = ExcelUtil.getOrCreateRowBySheetAndIndex(sheet, i);
                HSSFRow prevRow = sheet.getRow(i - 1);
                for (ExcelFormula formula : FormulaManager.getFormulasBySheet(sheetName)) {
                    HSSFCell prevCell = prevRow.getCell(formula.getColumn());
                    ExcelUtil.addValueToCell(ExcelUtil.generateDraggedFormula(prevCell.getCellFormula()), row,
                            formula.getColumn(), CellType.FORMULA);
                }
            }
        }
    }

    private static void renameSheets(HSSFWorkbook workbook) {
        // Rename 1st sheet
        workbook.setSheetName(ExcelASCIISheetTransformer.ASCII_SHEET_NUMBER, "ascii");
        // Rename 2nd sheet
        workbook.setSheetName(LINKS_SHEET_NUMBER, "links");
        // Rename 3rd sheet
        workbook.setSheetName(ExcelEdifactSheetTransformer.EDIFACT_SHEET_NUMBER, "edifact");
    }

    public static boolean cellHasSegmentIdentifierValue(HSSFCell segmentClassCell) {
        return (CellType.NUMERIC.equals(segmentClassCell.getCellTypeEnum())
                || CellType.FORMULA.equals(segmentClassCell.getCellTypeEnum()))
                && SEGMENT_IDENTIFIER_VALUE == segmentClassCell.getNumericCellValue();
    }
}

package com.mappingcreator.core.exceltransformer;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;

import com.mappingcreator.core.mapping.MappingDescriptor;
import com.mappingcreator.core.util.validation.ValidationMessage;

public class ExcelASCIISheetTransformer {

    public static final int ASCII_SHEET_NUMBER                = 0;
    public static final int ASCII_SEGMENT_CLASS_COLUMN        = 6;
    public static final int ASCII_INITIAL_SEGMENT_NAME_COLUMN = 9;
    public static final int ASCII_SEGMENT_GROUP_COLUMN        = 11;
    public static final int ASCII_ADDED_SEGMENT_NAME_COLUMN   = 12;

    public static void addASCIISheetGroupNames(MappingDescriptor md, List<ValidationMessage> errors,
            HSSFWorkbook workbook) {
        HSSFSheet asciiSheet = workbook.getSheetAt(ASCII_SHEET_NUMBER);

        for (int i = 0; i < asciiSheet.getLastRowNum(); i++) {
            HSSFRow row = ExcelUtil.getOrCreateRowBySheetAndIndex(asciiSheet, i);
            addSegmentGroupInfoToRow(md, errors, asciiSheet, i, row);
        }
    }

    private static void addSegmentGroupInfoToRow(MappingDescriptor md, List<ValidationMessage> errors,
            HSSFSheet sheet, int i, HSSFRow row) {
        // Segments are declared on lines having column SEGMENT_CLASS_COLUMN (6) as a numeric cell with value = 0
        HSSFCell classIndexCell = row.getCell(ASCII_SEGMENT_CLASS_COLUMN);
        if (classIndexCell != null && CellType.NUMERIC.equals(classIndexCell.getCellTypeEnum())
                && ExcelTransformer.cellHasSegmentIdentifierValue(classIndexCell)) {
            String segmentName = getASCIISegmentName(sheet, i, row);

            // For each segment, get the corresponding ASCII node
            String groupName = md.getAsciiDictionary().get(segmentName);
            if (groupName != null) {
                ExcelUtil.addStringValueToCell(groupName, row, ASCII_SEGMENT_GROUP_COLUMN);
                ExcelUtil.addStringValueToCell(segmentName, row, ASCII_ADDED_SEGMENT_NAME_COLUMN);
            } else {
                // There is no exact match for this segment in the ascii dictionary
                errors.add(new ValidationMessage("error.no-segment", new String[] { segmentName }));
            }
        }
    }

    /**
     * Get the segment name, depending on the main segment cell and optional segment type cell
     * @param sheet
     * @param i
     * @param row
     * @return
     */
    private static String getASCIISegmentName(HSSFSheet sheet, int i, HSSFRow row) {
        HSSFCell segmentNameCell = row.getCell(ASCII_INITIAL_SEGMENT_NAME_COLUMN);
        if (segmentNameCell == null) {
            throw new RuntimeException("Could not find segment name on row " + i + ", column "
                    + ASCII_INITIAL_SEGMENT_NAME_COLUMN);
        }
        String segmentName = segmentNameCell.getStringCellValue();
        HSSFRow nextRow = sheet.getRow(i + 1);
        if (nextRow != null) {
            HSSFCell segmentSubNameCell = nextRow.getCell(ASCII_INITIAL_SEGMENT_NAME_COLUMN);
            if (segmentSubNameCell != null) {
                segmentName += segmentSubNameCell.getStringCellValue();
            }
        }
        return segmentName;
    }
}

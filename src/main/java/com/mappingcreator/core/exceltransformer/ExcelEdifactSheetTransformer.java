package com.mappingcreator.core.exceltransformer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mappingcreator.core.mapping.EdifactDictionary;
import com.mappingcreator.core.mapping.MappingDescriptor;
import com.mappingcreator.core.util.validation.ValidationMessage;

public class ExcelEdifactSheetTransformer {

    private static final String UNH_SEGMENT                         = "UNH";

    public static final int     EDIFACT_SHEET_NUMBER                = 2;

    public static final int     EDIFACT_INITIAL_SEGMENT_NAME_COLUMN = 1;
    public static final int     EDIFACT_SEGMENT_CLASS_COLUMN        = 6;
    public static final int     EDIFACT_SEGMENT_GROUP_COLUMN        = 11;
    public static final int     EDIFACT_ADDED_SEGMENT_NAME_COLUMN   = 12;

    private static final Logger LOGGER                              = LoggerFactory
            .getLogger(ExcelEdifactSheetTransformer.class);

    public static void addEdifactSheetGroupNames(MappingDescriptor md, List<ValidationMessage> errors,
            HSSFWorkbook workbook) {
        HSSFSheet edifactSheet = workbook.getSheetAt(EDIFACT_SHEET_NUMBER);

        md.getEdifactDictionary().reset();
        for (int i = 0; i < edifactSheet.getLastRowNum(); i++) {
            HSSFRow row = ExcelUtil.getOrCreateRowBySheetAndIndex(edifactSheet, i);
            addSegmentGroupInfoToRow(md, errors, edifactSheet, i, row);
        }
    }

    private static void addSegmentGroupInfoToRow(MappingDescriptor md, List<ValidationMessage> errors,
            HSSFSheet edifactSheet, int i, HSSFRow row) {

        HSSFCell classIndexCell = row.getCell(EDIFACT_SEGMENT_CLASS_COLUMN);
        if (classIndexCell != null && ExcelTransformer.cellHasSegmentIdentifierValue(classIndexCell)) {

            HSSFCell segmentNameCell = row.getCell(EDIFACT_INITIAL_SEGMENT_NAME_COLUMN);
            SimpleEntry<String, String> groupSegmentPair = md.getEdifactDictionary().getCrtEntry();
            if (segmentNameCell.getStringCellValue() != null && groupSegmentPair.getKey()
                    .equalsIgnoreCase(segmentNameCell.getStringCellValue().substring(0, 3))) {
                ExcelUtil.addStringValueToCell(groupSegmentPair.getValue(), row,
                        EDIFACT_SEGMENT_GROUP_COLUMN);
                ExcelUtil.addStringValueToCell(groupSegmentPair.getKey(), row,
                        EDIFACT_ADDED_SEGMENT_NAME_COLUMN);
                md.getEdifactDictionary().increaseCounter();
            }
        }
    }

    public static EdifactDictionary extractEdiDictionaryStructure(String xlsFilePath) {
        List<SimpleEntry<String, String>> dictionary = new ArrayList<SimpleEntry<String, String>>();
        try (InputStream fis = new FileInputStream(new File(xlsFilePath));
                HSSFWorkbook workbook = new HSSFWorkbook(fis)) {
            HSSFSheet ediSheet = workbook.getSheetAt(EDIFACT_SHEET_NUMBER);
            for (int i = 0; i < ediSheet.getLastRowNum(); i++) {
                extractEdifactSegmentFromRow(dictionary, ediSheet, i);
            }

        } catch (FileNotFoundException fnfe) {
            LOGGER.error("File " + xlsFilePath + " was not found!", fnfe);
        } catch (IOException ioexc) {
            LOGGER.error("Unable to close stream for file " + xlsFilePath, ioexc);
        }
        removePreUNHNodes(dictionary);
        return new EdifactDictionary(dictionary);
    }

    private static void removePreUNHNodes(List<SimpleEntry<String, String>> dictionary) {
        // Remove all nodes before UNH
        Iterator<SimpleEntry<String, String>> iter = dictionary.iterator();
        while (iter.hasNext()) {
            SimpleEntry<String, String> entry = iter.next();
            if (entry.getKey().equalsIgnoreCase(UNH_SEGMENT)) {
                break;
            }
            iter.remove();
        }
    }

    private static void extractEdifactSegmentFromRow(List<SimpleEntry<String, String>> dictionary,
            HSSFSheet ediSheet, int rowNumber) {
        HSSFRow row = ExcelUtil.getOrCreateRowBySheetAndIndex(ediSheet, rowNumber);
        HSSFCell segmentClassCell = row.getCell(EDIFACT_SEGMENT_CLASS_COLUMN);
        if (segmentClassCell != null && ExcelTransformer.cellHasSegmentIdentifierValue(segmentClassCell)) {
            String segmentName = extractSegmentNameFromXLS(row);
            dictionary.add(new SimpleEntry<String, String>(segmentName, null));
        }
    }

    private static String extractSegmentNameFromXLS(HSSFRow row) {
        HSSFCell segmentNameCell = row.getCell(EDIFACT_INITIAL_SEGMENT_NAME_COLUMN);

        if (segmentNameCell == null) {
            throw new RuntimeException("Could not find segment name on row " + row.getRowNum() + ", column "
                    + EDIFACT_INITIAL_SEGMENT_NAME_COLUMN);
        }
        String segmentName = segmentNameCell.getStringCellValue().substring(0, 3);
        return segmentName;
    }
}

package com.mappingcreator.core.util.dbconnector;

import java.sql.Connection;
import java.sql.DriverManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteOpenMode;

import com.mappingcreator.core.util.GlobalConfig;

public class SQLiteConnectionCreator {
	private static final Logger LOGGER = LoggerFactory.getLogger(SQLiteConnectionCreator.class);

	public static Connection getConnection() {
		Connection c = null;
		try {
			SQLiteConfig config = new SQLiteConfig();
			config.setOpenMode(SQLiteOpenMode.NOMUTEX);
			config.setBusyTimeout("10000");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + GlobalConfig.get("dbPath"), config.toProperties());
		} catch (Exception e) {
			LOGGER.error("Unable to create connection", e);
		}
		return c;
	}
}

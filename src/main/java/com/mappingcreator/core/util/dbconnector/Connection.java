package com.mappingcreator.core.util.dbconnector;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface Connection extends AutoCloseable {

	public void close();

	public PreparedStatement prepareStatement(String queryToRun) throws SQLException;
}
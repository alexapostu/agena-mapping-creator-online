package com.mappingcreator.core.util.dbconnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.sqlite.SQLiteConnection;
import org.sqlite.jdbc4.JDBC4PreparedStatement;


@Repository
public class GenericDAO {

	protected static SanitizedConnection getConnection() {
		return new SanitizedConnection(ConnectionPool.getConnection());
	}

	public static void registerConnection(Connection con) {
		ConnectionPool.registerConnection(con);
	}
}

class SanitizedConnection implements com.mappingcreator.core.util.dbconnector.Connection {
	private static final Logger	LOGGER		= LoggerFactory.getLogger(SanitizedConnection.class);

	private Connection			connection;
	List<PreparedStatement>		statements	= new ArrayList<PreparedStatement>();

	SanitizedConnection(Connection connection) {
		this.connection = connection;
	}

	public boolean isClosed() {
		return connection == null;
	}

	public void close() {
		if (connection == null) {
			throw new IllegalStateException("Connection already closed!");
		}
		if (statements != null) {
			for (PreparedStatement statement : statements) {
				try {
					statement.close();
				} catch (SQLException exc) {
					LOGGER.error("Unhandled exception while closing statement", exc);
				}
			}
		}
		ConnectionPool.releaseConnection(connection);
		connection = null;
	}

	@Override
	public PreparedStatement prepareStatement(String queryToRun) throws SQLException {
		ManagedPreparedStatement mps = new ManagedPreparedStatement((SQLiteConnection) connection, queryToRun);

		statements.add(mps);
		return mps;
	}
}

class ManagedPreparedStatement extends JDBC4PreparedStatement {
	List<ResultSet> resultSets = new ArrayList<ResultSet>();

	public ManagedPreparedStatement(SQLiteConnection conn, String sql) throws SQLException {
		super(conn, sql);
	}

	public ResultSet executeQuery() throws SQLException {
		ResultSet rs = super.executeQuery();
		resultSets.add(rs);
		return rs;
	}

	public ResultSet getGeneratedKeys() throws SQLException {
		ResultSet rs = super.getGeneratedKeys();
		resultSets.add(rs);
		return rs;
	}

	public void close() throws SQLException {
		if (resultSets != null) {
			for (ResultSet rs : resultSets) {
				if (!rs.isClosed()) {
					rs.close();
				}
			}
		}
		super.close();
	}
}

/**
 * Connection Pool class - holds active connections to the database and allows
 * for them to be retrieved by processes
 * 
 * @author Alex
 *
 */
class ConnectionPool {

	private static final Deque<Connection>	AVAILABLE_CONNECTIONS	= new LinkedList<Connection>();
	private static final Deque<Connection>	USED_CONNECTIONS		= new LinkedList<Connection>();

	public static synchronized Connection getConnection() {
		if (AVAILABLE_CONNECTIONS.isEmpty()) {
			throw new IllegalStateException("ALL DB CONNECTION POOLS HAVE BEEN USED!");
		}
		Connection con = AVAILABLE_CONNECTIONS.pop();
		USED_CONNECTIONS.addLast(con);
		return con;
	}

	public static synchronized void releaseConnection(Connection con) {
		AVAILABLE_CONNECTIONS.addLast(con);
		USED_CONNECTIONS.remove(con);
	}

	public static synchronized void registerConnection(Connection con) {
		AVAILABLE_CONNECTIONS.addLast(con);
	}
}
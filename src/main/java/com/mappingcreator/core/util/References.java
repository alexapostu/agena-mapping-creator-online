package com.mappingcreator.core.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class References {
	// This is a trick to have Spring set a static reference to a managed
	// component
	@Autowired
	private MessageSource _messageSource;

	@PostConstruct
	public void initStaticMessageSource() {
		messageSource = this._messageSource;
	}

	private static MessageSource messageSource;

	public interface LocationType {
		public static final int	LOCAL	= 1;
		public static final int	FTP		= 2;
		public static final int	SFTP	= 3;
		public static final int	HTTP	= 4;
	}

	public interface JobType {
		public static final int	ONE_TIME	= 5;
		public static final int	PERIODIC	= 6;
	}

	public static List<Integer> getJobTypes() {
		return Arrays.asList(References.JobType.ONE_TIME, References.JobType.PERIODIC);
	}

	public interface TimeUnit {
		public static final int	SECOND	= 7;
		public static final int	MINUTE	= 8;
		public static final int	HOUR	= 9;
		public static final int	DAY		= 10;
		public static final int	MONTH	= 11;
		public static final int	YEAR	= 12;
	}

	public interface TaskType {
		public static final int MOVE_FILES = 13;
	}

	public static List<Integer> getTimeUnitTypes() {
		return Arrays.asList(TimeUnit.SECOND, TimeUnit.MINUTE, TimeUnit.HOUR, TimeUnit.DAY, TimeUnit.MONTH,
				TimeUnit.YEAR);
	}

	public interface Tables {
		public static final String	JOBS		= "jobs";
		public static final String	LOCATIONS	= "locations";
	}

	public static String translateReferenceList(List<Integer> refList) {
		StringJoiner sj = new StringJoiner(", ");
		for (int refId : refList) {
			String translatedVal = messageSource.getMessage("reference." + refId, new String[] {},
					LocaleContextHolder.getLocale());
			sj.add(translatedVal);
		}

		return sj.toString();
	}

	public static List<Integer> getTaskTypes() {
		return getStaticValuesFromInterface(TaskType.class);
	}

	private static List<Integer> getStaticValuesFromInterface(Class clazz) {
		List<Integer> listValues = new ArrayList<Integer>();
		Field[] fields = clazz.getDeclaredFields();
		try {
			for (Field f : fields) {
				if (Modifier.isStatic(f.getModifiers())) {
					listValues.add(f.getInt(null));
				}
			}
		} catch (Exception exc) {
			throw new UnexpectedException(exc);
		}
		return listValues;
	}
}

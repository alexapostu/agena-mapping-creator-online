package com.mappingcreator.core.util.validation;

public class ValidationMessage {
	private String		code;
	private String[]	params;

	public ValidationMessage(String code, String[] params) {
		super();
		this.code = code;
		this.params = params;
	}

	public ValidationMessage(String code) {
		this.code = code;
		this.params = new String[] {};
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String[] getParams() {
		return params;
	}

	public void setParams(String[] params) {
		this.params = params;
	}

}

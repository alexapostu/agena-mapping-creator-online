package com.mappingcreator.core.util.queryprocessing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mappingcreator.core.util.UnexpectedException;

public class QueryManager {
	private static Map<String, String>	queries	= new HashMap<String, String>();

	private static final Logger			LOGGER	= LoggerFactory.getLogger(QueryManager.class);

	private QueryManager() {
		// This constructor should prevent the external instantiation of this
		// utility class
	}

	public static String getQuery(String queryName) {
		return queries.get(queryName);
	}

	private void registerQuery(String queryName, String query) {
		if (queries.get(queryName) != null) {
			throw new UnexpectedException("Duplicate declaration of named query '" + queryName + "'");
		}
		queries.put(queryName, query);
	}

	private void registerQueryFile(File file) {
		QueryFileProcessor qfp = new QueryFileProcessor();
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line = br.readLine();

			while (line != null) {
				qfp.processLine(line);
				line = br.readLine();
			}
			qfp.processLine(">>END");
		} catch (Exception e) {
			LOGGER.error("Unable to load query file!");
			throw new UnexpectedException(e);
		}
	}

	public static void init() {
		QueryManager qm = new QueryManager();

		ClassLoader classLoader = qm.getClass().getClassLoader();
		
		// Directory resources\queries
		File queryDir = new File(classLoader.getResource("queries").getFile());
		if (queryDir != null && queryDir.isDirectory()) {
			for (String file : queryDir.list()) {
				String queryFilePath = "queries/" + file;
				qm.registerQueryFile(new File(classLoader.getResource(queryFilePath).getFile()));
			}
		}
	}

	private class QueryFileProcessor {
		private String	crtQueryName	= null;
		private String	crtQueryBody	= null;

		public void processLine(String originalLine) {
			String line = originalLine.trim();
			if (line.startsWith("#") || line.startsWith("--")) {
				// File contans a comment
				return;
			}
			if (line.startsWith(">>")) {
				// Query name declaration
				if (crtQueryName == null) {
					crtQueryName = line.substring(2);
				} else {
					if (crtQueryBody == null) {
						throw new UnexpectedException(
								"Declaration of query without a body starting with named query " + crtQueryName);
					}
					registerQuery(crtQueryName, crtQueryBody);
					crtQueryName = line.substring(2);
					crtQueryBody = null;
				}

			} else {
				if (crtQueryBody == null) {
					crtQueryBody = "";
				}
				crtQueryBody += line + " ";
			}
		}
	}
}

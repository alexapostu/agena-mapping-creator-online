package com.mappingcreator.core.util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class GlobalConfig {
	private static final Map<String, String> PROPS = new HashMap<String, String>();
	private static boolean ready = false;

	public static String get(String key) {
		if (!PROPS.containsKey(key)) {
			return "";
		}
		return PROPS.get(key);
	}

	public static void initGlobalConfig() {
		if (ready) {
			throw new IllegalStateException("Configuration files already initialized!");
		}
		getConfigFromCoreParams();
		String configPath = get("appConfigFile");
		getConfigFromExternalFile(configPath);

		ready = true;
	}

	private static void getConfigFromExternalFile(String extPath) {
		File extConfFile = new File(extPath);
		if (extConfFile.exists() && !extConfFile.isDirectory()) {
			addPropsFromFile(extConfFile);
		}
	}

	private static void getConfigFromCoreParams() {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		File file = new File(classLoader.getResource("coreConfig.properties").getFile());

		addPropsFromFile(file);

	}

	private static void addPropsFromFile(File file) {
		try (Scanner scanner = new Scanner(file)) {

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] result = line.split("=", 2);
				PROPS.put(result[0], result[1]);
			}

			scanner.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

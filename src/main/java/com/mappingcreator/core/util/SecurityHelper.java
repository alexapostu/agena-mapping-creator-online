package com.mappingcreator.core.util;

import org.apache.commons.codec.digest.DigestUtils;

public class SecurityHelper {
	public static String encodeSHA256(String upass) {
		return DigestUtils.sha256Hex(upass);
	}
}

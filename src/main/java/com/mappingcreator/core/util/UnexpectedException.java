package com.mappingcreator.core.util;

public class UnexpectedException extends RuntimeException {
    private static final long serialVersionUID = -9211840742703180349L;

    // Default runtime exception in the app - nothing special about it.
    public UnexpectedException(Throwable exc) {
        super(exc);
    }

    public UnexpectedException(String message) {
        super(message);
    }

    public UnexpectedException(String message, Throwable exc) {
        super(message, exc);
    }
}

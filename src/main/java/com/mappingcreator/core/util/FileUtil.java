package com.mappingcreator.core.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

public class FileUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

    public static List<File> getFilesInDirectory(String path, boolean searchSubdirs) {
        List<File> filesInDir = new ArrayList<File>();
        File sourceDir = new File(path);
        if (sourceDir.exists() && sourceDir.isDirectory()) {
            for (String filePath : sourceDir.list()) {
                File file = new File(FileUtil.concatPaths(sourceDir.getAbsolutePath(), filePath));
                if (!file.isDirectory()) {
                    filesInDir.add(file);
                } else if (searchSubdirs) {
                    filesInDir.addAll(getFilesInDirectory(file.getAbsolutePath(), searchSubdirs));
                }
            }
        }
        return filesInDir;
    }

    public static String subtractPaths(String basePath, String subtractPath) {
        String processedBase = basePath.toLowerCase().replaceAll("\\/", "\\\\").replaceAll("/+$", "");
        String processedSubtract = subtractPath.toLowerCase().replaceAll("\\/", "\\\\").replaceAll("/+$", "");
        String finalPath = processedBase.substring(processedBase.indexOf(processedSubtract));
        finalPath = finalPath.replace(processedSubtract, "");
        return finalPath;
    }

    public static String concatPaths(String dirPath, String subDirPath) {
        return dirPath.replaceAll("\\/+$|\\\\+$", "") + File.separator
                + subDirPath.replaceAll("^\\/+|^\\\\+", "");
    }

    public static void deleteDirectory(String directoryPath) {
        try {
            FileUtils.deleteDirectory(new File(directoryPath));
        } catch (IOException e) {
            LOGGER.error("Unable to delete directory at " + directoryPath, e);
        }
    }

    public static void deleteFileIfExists(String path) {
        File oldXML = new File(path);
        if (oldXML.exists()) {
            oldXML.delete();
        }
    }

    public static void writeToFile(String fileTextContent, String filePath) {
        File logFile = new File(filePath);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFile));) {
            writer.write(fileTextContent);
        } catch (IOException e) {
            LOGGER.error("Unable to write file!", e);
            e.printStackTrace();
        }
    }

    public static String uploadMultipartFileToPath(MultipartFile file, String fileName) {
        try (InputStream is = file.getInputStream()) {
            Files.copy(is, Paths.get(fileName));
        } catch (IOException e) {
            LOGGER.error("Unable to upload file", e);
        }
        return fileName;
    }
}

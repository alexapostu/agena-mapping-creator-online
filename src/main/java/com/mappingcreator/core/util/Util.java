package com.mappingcreator.core.util;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Util {
	private static final Logger	LOGGER		= LoggerFactory.getLogger(Util.class);

	public static final String	DATE_REGEX	= "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";
	public static final String	TIME_REGEX	= "^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$";

	public static Integer stringToInteger(String formValue, Integer defaultVal) {
		try {
			return Integer.valueOf(formValue);
		} catch (NumberFormatException nfe) {
			return defaultVal;
		}
	}

	public static Date longToDate(long intToConvert) {
		if (intToConvert <= 0) {
			return null;
		}

		return new Date(intToConvert);
	}

	public static Date stringToDate(String dateString) {
		try {
			return (new SimpleDateFormat("dd/MM/yyyy HH:mm")).parse(dateString);
		} catch (ParseException e) {
			LOGGER.debug("Unable to cast string `" + dateString + "` to date");
		}
		return null;
	}

	public static boolean getBoolFromCheckbox(String checkBoxValue) {
		return "on".equalsIgnoreCase(checkBoxValue);
	}

	public static void trySleep(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException exc) {
			LOGGER.error("Could not sleep for " + millis + " millis", exc);
		}
	}

    public static String getCompleteResourcePath(String resourcePath) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        File file = new File(classLoader.getResource(resourcePath).getFile());
        return file.getAbsolutePath();
    }
}

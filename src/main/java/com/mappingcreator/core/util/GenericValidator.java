package com.mappingcreator.core.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import com.mappingcreator.core.util.validation.ValidationMessage;

@Component
public class GenericValidator {
    protected List<ValidationMessage>       result = new ArrayList<ValidationMessage>();
    protected MultiValueMap<String, String> formData;

    protected void rejectFieldEmpty(String fieldName) {
        String formValue = formData.getFirst(fieldName);
        if (formValue == null || formValue.isEmpty()) {
            result.add(new ValidationMessage("validation.empty-mandatory-parameter",
                    new String[] { fieldName }));
        }
    }

    protected void checkInteger(String fieldName) {
        String formValue = formData.getFirst(fieldName);
        if (Util.stringToInteger(formValue, null) == null) {
            result.add(new ValidationMessage("validation.mandatory-integer", new String[] { fieldName }));
        }

    }

    protected void rejectFieldNotInList(String fieldName, List<Integer> refList) {
        String formValue = formData.getFirst(fieldName);
        int refCode = 0;
        if (Util.stringToInteger(formValue, null) != null) {
            refCode = Util.stringToInteger(formValue, null);
        }
        if (!refList.contains(refCode)) {
            result.add(new ValidationMessage("validation.value-in-list",
                    new String[] { fieldName, References.translateReferenceList(refList) }));
        }

    }

    /**
     * Reject a value if it does not match an expected regexToMatch.
     * 
     * @param fieldName
     *            the field that needs to be matched
     * @param regexToMatch
     *            the regex pattern expected from this field
     * @param format
     *            the format interpretation of this regex, used in the
     *            validation message. If ommited, it defaults to regexToMatch
     */
    protected void rejectFieldNoMatch(String fieldName, String regexToMatch, String format) {
        String formValue = formData.getFirst(fieldName);
        if (formValue != null && formValue.matches(regexToMatch)) {
            return;
        }
        if (format == null) {
            format = regexToMatch;
        }
        result.add(new ValidationMessage("validation.regex-match", new String[] { fieldName, format }));
    }

    /**
     * Reject a value if it is not the identifier (id) in a table.
     * 
     * @param fieldName
     *            The field name for the value that will be searched
     * @param tableName
     *            The table where the field will be searched
     */
    protected void rejectEntryNotInDBTable(String fieldName, String tableName) {
        int formValue = Util.stringToInteger(formData.getFirst(fieldName), -1);
        if (!GeneralPurposeDAO.isFieldInTable(formValue, tableName)) {
            result.add(new ValidationMessage("validation.value-not-in-table",
                    new String[] { String.valueOf(formValue), tableName }));
        }
    }

    /**
     * Reject 2 fields that have the same value
     * 
     * @param string
     * @param string2
     */
    protected void rejectEqualFields(String field1, String field2) {
        String field1Val = formData.getFirst(field1);
        String field2Val = formData.getFirst(field2);
        if (field1Val != null && field1Val.equals(field2Val)) {
            result.add(new ValidationMessage("validation.values-are-equal", new String[] { field1, field2 }));
        }
    }
}

package com.mappingcreator.core.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mappingcreator.core.util.dbconnector.Connection;
import com.mappingcreator.core.util.dbconnector.GenericDAO;

public class GeneralPurposeDAO extends GenericDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneralPurposeDAO.class);

    public static boolean isFieldInTable(int id, String table) {
        try (Connection con = getConnection()) {
            String sanitizedTableName = table.replaceAll("[^A-Za-z0-9\\-\\_]", "");
            String query = "SELECT count(*) as nrOfEntities FROM " + sanitizedTableName + " WHERE id = ?";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getInt("nrOfEntities") > 0;
            }

        } catch (SQLException exc) {
            LOGGER.error("Unable to execute query", exc);
            throw new UnexpectedException(exc);
        }
        return false;
    }
}

package com.mappingcreator.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionUtil.class);

    public static void error(String error) {
        LOGGER.error(error);
        throw new UnexpectedException(error);
    }
    public static void error(String error, Throwable t) {
        LOGGER.error(error);
        throw new UnexpectedException(error, t);
    }

}

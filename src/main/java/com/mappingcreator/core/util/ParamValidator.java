package com.mappingcreator.core.util;

import java.util.List;

import org.springframework.util.MultiValueMap;

import com.mappingcreator.core.util.validation.ValidationMessage;

public interface ParamValidator {
	public List<ValidationMessage> validate(MultiValueMap<String, String> params);
}

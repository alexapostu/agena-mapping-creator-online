package com.mappingcreator.core.menu;

import java.util.List;

public class MenuDescriptor {
	private static MenuDescriptor instance;
	private List<MenuItem> menuItems;

	private MenuDescriptor() {
		// This private constructor should prevent the instantiation of this
		// Singleton class
	}

	public static MenuDescriptor getInstance() {
		if (instance == null) {
			instance = new MenuDescriptor();
		}
		return instance;
	}

	public List<MenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(List<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	public static void init() {
		MenuDigester.buildMenu();
	}
}

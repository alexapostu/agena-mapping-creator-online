package com.mappingcreator.core.menu;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.mappingcreator.core.util.Util;

public class MenuDigester {
    private static final Logger LOGGER             = LoggerFactory.getLogger(MenuDigester.class);
    private static final String PATH_TO_DESCRIPTOR = "menu/menu_descriptor.xml";
    private static final String MENU_LINK          = "link";
    private static final String MENU_ICON          = "icon";
    private static final String MENU_PERMISSION    = "permission";
    private static final String MENU_LABEL         = "label";

    public static void buildMenu() {
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            File file = new File(classLoader.getResource(PATH_TO_DESCRIPTOR).getFile());

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList nodes = (NodeList) xPath.evaluate("/menu/item", doc.getDocumentElement(),
                    XPathConstants.NODESET);
            MenuDescriptor.getInstance().setMenuItems(getMenuItems(nodes));

        } catch (Exception exc) {
            LOGGER.error("Unable to process menu descriptor file.", exc);
        }

    }

    private static List<MenuItem> getMenuItems(NodeList nodes) {
        List<MenuItem> items = new ArrayList<MenuItem>();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node xmlNode = nodes.item(i);
            // This library sees many things as nodes, remove those that are not
            // elements
            if (xmlNode.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            MenuItem item = new MenuItem();
            processAttributes(item, xmlNode);
            if (xmlNode.hasChildNodes()) {
                item.setChildItems(getMenuItems(xmlNode.getChildNodes()));
            }
            items.add(item);
        }

        return items;
    }

    private static void processAttributes(MenuItem item, Node xmlNode) {
        for (int i = 0; i < xmlNode.getAttributes().getLength(); i++) {
            Node attrib = xmlNode.getAttributes().item(i);

            switch (attrib.getNodeName()) {
                case MENU_LINK:
                    item.setLink(attrib.getNodeValue());
                    break;
                case MENU_ICON:
                    item.setIcon(attrib.getNodeValue());
                    break;
                case MENU_LABEL:
                    item.setLabel(attrib.getNodeValue());
                    break;
                case MENU_PERMISSION:

                    item.setPermissionId(Util.stringToInteger(attrib.getNodeValue(), null));
                    break;
            }
        }
    }
}

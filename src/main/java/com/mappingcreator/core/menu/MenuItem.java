package com.mappingcreator.core.menu;

import java.util.List;

public class MenuItem {
	private List<MenuItem> childItems;
	private String icon;
	private String label;
	private String link;
	private Integer permissionId;

	public List<MenuItem> getChildItems() {
		return childItems;
	}

	public void setChildItems(List<MenuItem> childItems) {
		this.childItems = childItems;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Integer getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Integer permissionId) {
		this.permissionId = permissionId;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
}

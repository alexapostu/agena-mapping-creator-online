package com.mappingcreator.core.users;

public class User {
	private int id;
	private String login;
	private String passHash;
	private String name;
	
	public User(int id, String login, String passHash, String name) {
		super();
		this.id = id;
		this.login = login;
		this.passHash = passHash;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassHash() {
		return passHash;
	}

	public void setPassHash(String passHash) {
		this.passHash = passHash;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

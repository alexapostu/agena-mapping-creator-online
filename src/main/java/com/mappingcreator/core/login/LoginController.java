package com.mappingcreator.core.login;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
	@Autowired
	LoginService loginService;

	@RequestMapping(value = "/login")
	public String getLoginPage(Model model) {
		return "templates/login";
	}

	@RequestMapping(value = "/login/doLogin")
	public String doLogin(HttpSession session, @RequestBody MultiValueMap<String, String> formData, Model model) {
		if (loginService.canLogUser(formData, model, session)) {
			return "redirect:/";
		}
		return "templates/login";
	}

	@GetMapping("/logout")
	public String doLogout(HttpSession session) {
		loginService.doLogout(session);
		return "redirect:/login";

	}
}

package com.mappingcreator.core.login;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;

import com.mappingcreator.core.users.User;
import com.mappingcreator.core.util.SecurityHelper;
import com.mappingcreator.core.util.validation.ValidationMessage;

@Service
public class LoginService {
	@Autowired
	LoginDAO		loginDAO;

	@Autowired
	LoginValidator	loginValidator;

	public boolean canLogUser(MultiValueMap<String, String> formData, Model model, HttpSession session) {
		List<ValidationMessage> messages = loginValidator.validate(formData);

		if (messages.isEmpty()) {
			User user = loginDAO.tryLogUser(formData.getFirst("uname"),
					SecurityHelper.encodeSHA256(formData.getFirst("upass")));
			if (user == null) {
				messages.add(new ValidationMessage("login.error.incorrect_user_or_pass"));
			} else {
				session.setAttribute("USER", user);
				return true;
			}
		}

		model.addAttribute("errors", messages);
		return false;
	}

	public void doLogout(HttpSession session) {
		session.setAttribute("USER", null);
	}

}

package com.mappingcreator.core.login;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import com.mappingcreator.core.util.GenericValidator;
import com.mappingcreator.core.util.validation.ValidationMessage;

@Component
public class LoginValidator extends GenericValidator {

    public List<ValidationMessage> validate(MultiValueMap<String, String> formData) {
        this.result = new ArrayList<ValidationMessage>();
        this.formData = formData;

        rejectFieldEmpty("uname");
        rejectFieldEmpty("upass");

        return result;
    }
}

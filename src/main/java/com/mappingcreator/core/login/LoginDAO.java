package com.mappingcreator.core.login;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.mappingcreator.core.users.User;
import com.mappingcreator.core.util.dbconnector.Connection;
import com.mappingcreator.core.util.dbconnector.GenericDAO;
import com.mappingcreator.core.util.queryprocessing.QueryManager;

@Repository
public class LoginDAO extends GenericDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginDAO.class);

	public User tryLogUser(String uname, String upassHash) {
		try (Connection con = getConnection()) {
			PreparedStatement stm = con.prepareStatement(QueryManager.getQuery("USER_BY_LOGIN_AND_PASS"));
			stm.setString(1, uname);
			stm.setString(2, upassHash);

			ResultSet rs = stm.executeQuery();
			if (rs.next()) {
				return new User(rs.getInt("id"), rs.getString("login"), rs.getString("pass_hash"),
						rs.getString("name"));
			}

		} catch (SQLException e) {
			LOGGER.error("Unable to execute query", e);
			throw new RuntimeException("Unable to execute query", e);
		}
		return null;
	}

}

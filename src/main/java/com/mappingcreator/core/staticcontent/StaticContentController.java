package com.mappingcreator.core.staticcontent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.HandlerMapping;

@Controller
public class StaticContentController {

    @Autowired
    private StaticContentService staticContentService;

    @RequestMapping(value = "/static/**", method = RequestMethod.GET)
    @ResponseBody
    public void getStaticResource(HttpServletRequest request, HttpServletResponse response) {
        String fileName = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        fileName = fileName.replace("/static", "");

        staticContentService.setStaticResponse(response, fileName);
    }
}

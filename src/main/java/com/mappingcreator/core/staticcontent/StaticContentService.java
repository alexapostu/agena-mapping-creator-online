package com.mappingcreator.core.staticcontent;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.mappingcreator.core.util.FileUtil;
import com.mappingcreator.core.util.GlobalConfig;

@Service
public class StaticContentService {
    private static final Logger LOGGER = LoggerFactory.getLogger(StaticContentService.class);

    public void setStaticResponse(HttpServletResponse response, String fileName) {
        String fullFilePath = FileUtil.concatPaths(GlobalConfig.get("baseDir"), fileName);
        File fileToDownload = new File(fullFilePath);
        if (fileToDownload.exists()) {
            try (InputStream is = new FileInputStream(fileToDownload);) {
                IOUtils.copy(is, response.getOutputStream());
                response.flushBuffer();
            } catch (IOException ex) {
                LOGGER.error("Error writing file to output stream. Filename was '{}'", fileName, ex);
                throw new RuntimeException("IOError writing file to output stream");
            }
        }
    }
}

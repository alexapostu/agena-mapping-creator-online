package com.mappingcreator.core.mapping;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import com.mappingcreator.core.exceltransformer.ExcelEdifactSheetTransformer;
import com.mappingcreator.core.exceltransformer.ExcelTransformer;
import com.mappingcreator.core.util.FileUtil;
import com.mappingcreator.core.util.GlobalConfig;
import com.mappingcreator.core.util.UnexpectedException;
import com.mappingcreator.core.util.Util;

import generated.Root;
import net.sf.saxon.Transform;

@Service
public class MappingService {

    private static final String MODIFIED_EXTENSION          = "-modified";
    private static final String TEMP_XML_EXTENSION          = "temp.xml";

    @Autowired
    private MappingDAO          mappingDAO;

    private static final Logger LOGGER                      = LoggerFactory.getLogger(MappingService.class);

    private static final String TRANSFORM_MAPPING_FILE_NAME = "transformations/transform.xslt";
    private static final String TRANSFORM_HTML_FILE_NAME    = "transformations/transformHtml.xslt";
    private static final String MESSAGE_START               = "<div class=\"msg_struct_descr\">";
    private static final String MESSAGE_END                 = "<h3>Message structure</h3>";
    private static final String DUMB_PARAGRAPHS             = "</div></p>";
    private static final String LESS_DUMB_PARAGRAPHS        = "</div>";
    private static final String XML_EXTENSION               = ".xml";
    private static final String XLS_EXTENSION               = ".xls";

    public void generateMapping(MappingDescriptor md, HttpServletRequest request) {

        request.setAttribute("mappingId", generateXMLMapping(md));
        generateFinalFiles(md, request);
        cleanupTempFiles(md.getFullDirectoryPath());
    }

    /**
     * Generate updated XML (duplicate segments + groups) and XLS files
     * @param md
     * @param request
     */
    private void generateFinalFiles(MappingDescriptor md, HttpServletRequest request) {
        try {
            populateEdifactDictionary(md,
                    FileUtil.concatPaths(md.getFullDirectoryPath(), md.getDirectoryName()) + XML_EXTENSION);

        } catch (Exception exc) {
            throw new UnexpectedException("Exception trying to associate output segments", exc);
        }

        request.setAttribute("errors",
                ExcelTransformer.generateNewXLS(md.getXlsPath() + MODIFIED_EXTENSION + XLS_EXTENSION, md));
    }

    private int generateXMLMapping(MappingDescriptor md) {
        String mappingXML = getMessageFromWebpage(md.getFactoryURL());
        mappingXML = mappingXML.replaceAll(DUMB_PARAGRAPHS, LESS_DUMB_PARAGRAPHS);
        mappingXML = mappingXML.replaceAll("&", "&amp;");

        FileUtil.writeToFile(mappingXML, FileUtil.concatPaths(md.getFullDirectoryPath(), TEMP_XML_EXTENSION));
        try {
            String newFilePath = FileUtil.concatPaths(md.getFullDirectoryPath(), md.getDirectoryName());
            generateXMLMapping(newFilePath, md.getFullDirectoryPath());
            generateHTMLMapping(newFilePath);

            return mappingDAO.registerNewMapping(md);
        } catch (Exception exc) {
            LOGGER.error("Unable to complete operation", exc);
        }
        return 0;
    }

    private void populateEdifactDictionary(MappingDescriptor md, String filePath) throws Exception {
        String xml;
        try (FileInputStream inputStream = new FileInputStream(filePath)) {
            xml = IOUtils.toString(inputStream);
        }

        JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(xml);
        Root root = (Root) unmarshaller.unmarshal(reader);
        XMLDescriptorProcessor.populateDictionaryFromNodeList(md.getEdifactDictionary(),
                root.getSegmentOrGroup(), "");

        // Marshal newly formed XML
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(root, new File(filePath + MODIFIED_EXTENSION + XML_EXTENSION));
    }

    public MappingDescriptor generateMappingDescriptor(MultipartFile xlsFile, MultipartFile asciiDictionary,
            String url, String name) {

        MappingDescriptor md = new MappingDescriptor();
        return generateMappingDescriptor(xlsFile, asciiDictionary, url, name, md);
    }

    public MappingDescriptor generateMappingDescriptor(MultipartFile xlsFile, MultipartFile asciiDictionary,
            String url, String name, MappingDescriptor md) {
        String baseDirPath = FileUtil.concatPaths(GlobalConfig.get("baseDir"), name);
        (new File(baseDirPath)).mkdirs();
        md.setFullDirectoryPath(baseDirPath);

        uploadFiles(xlsFile, asciiDictionary, baseDirPath, md);
        generateMappingDescriptor(url, name, md);

        return md;
    }

    private void generateMappingDescriptor(String url, String name, MappingDescriptor md) {
        md.setDirectoryName(name);
        md.setFactoryURL(url);
        if (md.getXlsPath() != null) {
            md.setEdifactDictionary(
                    ExcelEdifactSheetTransformer.extractEdiDictionaryStructure(md.getXlsPath()));
        }

        if (md.getDictionaryName() != null) {
            String dictionaryPath = FileUtil.concatPaths(md.getFullDirectoryPath(), md.getDictionaryName());
            md.setAsciiDictionary(SQLDictionaryProcessor.extractASCIIDictionary(dictionaryPath));
        }
    }

    private void uploadFiles(MultipartFile xlsFile, MultipartFile asciiDictionary, String baseDirPath,
            MappingDescriptor md) {
        if (xlsFile != null && xlsFile.getSize() > 0) {
            md.setXlsPath(uploadXLSToDir(xlsFile, baseDirPath));
            md.setXlsName(xlsFile.getOriginalFilename());
        }

        if (asciiDictionary != null && asciiDictionary.getSize() > 0) {
            uploadFileToDir(asciiDictionary, md.getFullDirectoryPath());
            md.setDictionaryName(asciiDictionary.getOriginalFilename());
        }
    }

    private String uploadXLSToDir(MultipartFile file, String fullDirectoryPath) {
        String fileName = uploadFileToDir(file, fullDirectoryPath);
        try {
            Files.copy(Paths.get(fileName), Paths.get(fileName + MODIFIED_EXTENSION + XLS_EXTENSION));
        } catch (IOException e) {
            LOGGER.error("Unable to upload file", e);
        }
        return fileName;
    }

    private String uploadFileToDir(MultipartFile file, String fullDirectoryPath) {
        String filePath = FileUtil.concatPaths(fullDirectoryPath, file.getOriginalFilename());
        return FileUtil.uploadMultipartFileToPath(file, filePath);
    }

    private static void cleanupTempFiles(String baseDirPath) {
        (new File(FileUtil.concatPaths(baseDirPath, TEMP_XML_EXTENSION))).delete();
    }

    private static void generateXMLMapping(String fileName, String baseDirPath) throws Exception {
        String resourcePath = Util.getCompleteResourcePath(TRANSFORM_MAPPING_FILE_NAME);
        String[] arglist = { "-o:" + fileName + XML_EXTENSION,
                FileUtil.concatPaths(baseDirPath, TEMP_XML_EXTENSION), resourcePath };

        Transform.main(arglist);
    }

    private static void generateHTMLMapping(String fileName) throws Exception {
        String resourcePath = Util.getCompleteResourcePath(TRANSFORM_HTML_FILE_NAME);
        String[] arglist = { "-o:" + fileName + ".html", fileName + XML_EXTENSION, resourcePath };

        Transform.main(arglist);
    }

    private static String getMessageFromWebpage(String mappingURL) {
        String mappingXML = null;
        URL url;
        InputStream is = null;
        BufferedReader br;
        String line;

        try {
            url = new URL(mappingURL);
            is = url.openStream(); // throws an IOException
            br = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            if (sb.indexOf(MESSAGE_END) != -1) {
                sb.setLength(sb.indexOf(MESSAGE_END));
            }
            mappingXML = sb.substring(sb.indexOf(MESSAGE_START));

        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null)
                    is.close();
            } catch (IOException ioe) {
                // nothing to see here
            }
        }
        return mappingXML;
    }

    public static String concatPaths(String dirPath, String subDirPath) {
        return dirPath.replaceAll("\\/+$|\\\\+$", "") + File.separator
                + subDirPath.replaceAll("^\\/+|^\\\\+", "");
    }

    public void addHomeParamsToModel(Model model) {

        model.addAttribute("mappings", mappingDAO.getAllMappings());
    }

    public void addMappingInfoToModel(Model model, int mappingId) {
        model.addAttribute("mapping", mappingDAO.getMappingById(mappingId));

    }

    public void deleteMappingById(Integer mappingId) {
        MappingDescriptor mapping = mappingDAO.getMappingById(mappingId);
        deleteBaseDirectoryByName(mapping.getDirectoryName());
        mappingDAO.deleteMappingById(mappingId);
    }

    private void deleteBaseDirectoryByName(String directoryName) {
        String completePathToDir = FileUtil.concatPaths(GlobalConfig.get("baseDir"), directoryName);
        FileUtil.deleteDirectory(completePathToDir);
    }

    public void replaceXMLAndReprocess(MultipartFile xmlFile, int mappingId, HttpServletRequest request) {
        MappingDescriptor md = mappingDAO.getMappingById(mappingId);
        generateMappingDescriptor(null, null, md.getFactoryURL(), md.getDirectoryName(), md);
        replaceXML(xmlFile, md);
        generateFinalFiles(md, request);
    }

    private void replaceXML(MultipartFile xmlFile, MappingDescriptor md) {
        String xmlPath = FileUtil.concatPaths(md.getFullDirectoryPath(), md.getDirectoryName()) + ".xml";
        FileUtil.deleteFileIfExists(xmlPath);
        FileUtil.deleteFileIfExists(xmlPath + MODIFIED_EXTENSION + XML_EXTENSION);
        FileUtil.uploadMultipartFileToPath(xmlFile, xmlPath);
    }
}

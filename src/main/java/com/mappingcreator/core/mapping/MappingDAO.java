package com.mappingcreator.core.mapping;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.mappingcreator.core.util.UnexpectedException;
import com.mappingcreator.core.util.dbconnector.Connection;
import com.mappingcreator.core.util.dbconnector.GenericDAO;
import com.mappingcreator.core.util.queryprocessing.QueryManager;

@Repository
public class MappingDAO extends GenericDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(MappingDAO.class);

    public List<MappingDescriptor> getAllMappings() {
        List<MappingDescriptor> allMappings = new ArrayList<MappingDescriptor>();
        try (Connection con = getConnection()) {
            PreparedStatement stmt = con.prepareStatement(QueryManager.getQuery("GET_ALL_MAPPINGS"));
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                allMappings.add(getMappingFromResultSet(rs));
            }

        } catch (SQLException exc) {
            LOGGER.error("Unable to execute query", exc);
            throw new UnexpectedException(exc);
        }

        return allMappings;
    }

    private MappingDescriptor getMappingFromResultSet(ResultSet rs) throws SQLException {
        MappingDescriptor m = new MappingDescriptor();
        m.setDirectoryName(rs.getString("name"));
        m.setId(Integer.valueOf(rs.getString("id")));
        m.setXlsName(rs.getString("xlsName"));
        m.setXlsPath(rs.getString("xlsPath"));
        m.setFactoryURL(rs.getString("schemaURL"));
        m.setDictionaryName(rs.getString("inputDictionaryName"));
        return m;
    }

    public int registerNewMapping(MappingDescriptor md) {
        try (Connection con = getConnection()) {
            PreparedStatement stmt = con.prepareStatement(QueryManager.getQuery("INSERT_MAPPING"));
            stmt.setString(1, md.getDirectoryName());
            stmt.setString(2, md.getXlsName());
            stmt.setString(3, md.getXlsPath());
            stmt.setString(4, md.getFactoryURL());
            stmt.setString(5, md.getDictionaryName());
            stmt.execute();
            try (ResultSet rs = stmt.getGeneratedKeys()) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }

        } catch (SQLException exc) {
            LOGGER.error("Unable to execute query", exc);
            throw new UnexpectedException(exc);
        }

        return 0;
    }

    public MappingDescriptor getMappingById(int mappingId) {
        try (Connection con = getConnection()) {
            PreparedStatement stmt = con.prepareStatement(QueryManager.getQuery("GET_MAPPING_BY_ID"));
            stmt.setInt(1, mappingId);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return getMappingFromResultSet(rs);
            }

        } catch (SQLException exc) {
            LOGGER.error("Unable to execute query", exc);
            throw new UnexpectedException(exc);
        }
        return null;
    }

    public void deleteMappingById(Integer mappingId) {
        try (Connection con = getConnection()) {
            PreparedStatement stmt = con.prepareStatement(QueryManager.getQuery("DELETE_MAPPING_BY_ID"));
            stmt.setInt(1, mappingId);
            stmt.execute();

        } catch (SQLException exc) {
            LOGGER.error("Unable to execute query", exc);
            throw new UnexpectedException(exc);
        }
    }
}

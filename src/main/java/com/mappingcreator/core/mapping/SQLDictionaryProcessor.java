package com.mappingcreator.core.mapping;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQLDictionaryProcessor {

    private static final String SEGMENT_IDENTIFIER = "I";
    private static final String GROUP_IDENTIFIER   = "G";

    private static final Logger LOGGER             = LoggerFactory.getLogger(SQLDictionaryProcessor.class);

    public static Map<String, String> extractASCIIDictionary(String inputDirFile) {
        Map<String, String> dictionary = new HashMap<String, String>();
        try (BufferedReader br = new BufferedReader(new FileReader(inputDirFile))) {
            String line = br.readLine();
            while (line != null) {
                addSegmentFromLine(dictionary, line);
                line = br.readLine();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return dictionary;
    }

    private static void addSegmentFromLine(Map<String, String> dictionary, String line) {
        String[] composingEls = line.split("\\'");
        if (composingEls.length > 21) {
            switch (composingEls[3]) {
                case SEGMENT_IDENTIFIER:
                    String segmentName = composingEls[5];
                    String fullGroupName = composingEls[21];
                    dictionary.put(segmentName, getGroupNameFromFullSegmentPath(fullGroupName));
                    break;
            }
        }
    }

    private static String getGroupNameFromFullSegmentPath(String fullGroupName) {
        return fullGroupName.substring(0, fullGroupName.lastIndexOf("."));
    }
}

package com.mappingcreator.core.mapping;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class MappingController {

    @Autowired
    private MappingService mappingService;

    @RequestMapping(value = "/")
    public String home(Model model) {

        mappingService.addHomeParamsToModel(model);
        return "templates/home";
    }

    @RequestMapping(value = "/mapping/new")
    public String generateView(Model model) {
        return "templates/add_mapping";
    }

    @RequestMapping(value = "/mapping/generate")
    public String generateMapping(HttpServletRequest request, @RequestParam("xlsFile") MultipartFile xlsFile,
            @RequestParam("asciiDictionary") MultipartFile asciiDictionary,
            @RequestParam("mapping-url") String url, @RequestParam("mapping-name") String name) {

        MappingDescriptor md = mappingService.generateMappingDescriptor(xlsFile, asciiDictionary, url, name);
        mappingService.generateMapping(md, request);

        return "forward:/mapping/view";
    }

    @RequestMapping(value = "/mapping/view")
    public String viewMapping(HttpServletRequest request, Model model,
            @RequestParam(value = "mappingId", required = false) Integer mid) {

        Integer mappingId = (Integer) request.getAttribute("mappingId");
        if (mappingId == null) {
            mappingId = mid;
        }
        mappingService.addMappingInfoToModel(model, mappingId);

        model.addAttribute("errors", request.getAttribute("errors"));
        return "templates/show_mapping";
    }

    @RequestMapping(value = "/mapping/delete")
    public String deleteMapping(@RequestParam(value = "mappingId", required = true) Integer mappingId) {

        mappingService.deleteMappingById(mappingId);
        return "redirect:/";
    }

    @RequestMapping(value = "/mapping/changeXML")
    public String generateMapping(HttpServletRequest request, Model model,
            @RequestParam("xmlFile") MultipartFile xmlFile, @RequestParam("mappingId") int mappingId) {

        mappingService.replaceXMLAndReprocess(xmlFile, mappingId, request);
        request.setAttribute("mappingId", mappingId);

        return "forward:/mapping/view";
    }
}

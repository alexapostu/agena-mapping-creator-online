package com.mappingcreator.core.mapping;

import java.util.ArrayList;
import java.util.List;

import generated.Group;
import generated.ObjectFactory;
import generated.Segment;

public class XMLDescriptorProcessor {

    public static void populateDictionaryFromNodeList(EdifactDictionary edifactDictionary,
            List<Object> nodeList, String crtPath) {
        if (!edifactDictionary.isComplete()) {
            int i = 0;
            while (i < nodeList.size()) {
                Object node = nodeList.get(i);
                if (node instanceof Segment) {
                    processSegment((Segment) node, edifactDictionary, crtPath, nodeList, i);
                } else if(node instanceof Group){
                    processGroup((Group) node, edifactDictionary, crtPath, nodeList, i);
                }
                i++;
            }
        }
    }

    private static void processGroup(Group node, EdifactDictionary edifactDictionary, String crtPath,
            List<Object> nodeList, int elementIndex) {
        if (node.getMandatorySegment() != null && node.getMandatorySegment().getValue().equals(edifactDictionary.getCrtEntry().getKey())
                || node.isVisited()) {
            node.setVisited(true);
            String newPath = crtPath.length() > 0 ? crtPath + "." + node.getName() : node.getName();

            populateDictionaryFromNodeList(edifactDictionary, node.getContent(), newPath);
            
            // duplicate groups on mandatory segment duplication
            if (node.getMandatorySegment() != null && node.getMandatorySegment().getValue().equals(edifactDictionary.getCrtEntry().getKey())) {
                // Clone group
                nodeList.add(elementIndex + 1, ObjectFactory.deepCloneGroup(node));
            }
        }
    }

    private static void processSegment(Segment node, EdifactDictionary edifactDictionary, String crtPath,
            List<Object> nodeList, int elementIndex) {
        if (node.getValue().equals(edifactDictionary.getCrtEntry().getKey())) {
            edifactDictionary.getCrtEntry().setValue(crtPath);
            edifactDictionary.increaseCounter();

            int i = 1;
            while (node.getValue().equals(edifactDictionary.getCrtEntry().getKey())
                    && (node.getType().equals(Segment.TYPE_OPTIONAL) || "".equals(crtPath))) {
                // Create a group for this new sibling
                Group newGroup = new Group(new ArrayList<Object>(), wrapIndex(node.getValue(), i + 1),
                        Group.TYPE_OPTIONAL, 1, "", null);
                newGroup.getContent().add(ObjectFactory.cloneSegment(node));
                nodeList.add(elementIndex + i, newGroup);

                edifactDictionary.getCrtEntry()
                        .setValue("".equals(crtPath) ? newGroup.getName() : crtPath + "." + newGroup.getName());
                edifactDictionary.increaseCounter();
                i++;
            }
        }
    }

    private static String wrapIndex(String baseName, int index) {
        return baseName + "(" + index + ")";
    }

}

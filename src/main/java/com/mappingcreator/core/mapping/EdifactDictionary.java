package com.mappingcreator.core.mapping;

import java.util.AbstractMap.SimpleEntry;
import java.util.List;

public class EdifactDictionary {
    private List<SimpleEntry<String, String>> entries;
    private int                               index = 0;

    public EdifactDictionary(List<SimpleEntry<String, String>> entries) {
        this.entries = entries;
    }

    public SimpleEntry<String, String> getCrtEntry() {
        if (index >= 0 && index < entries.size()) {
            return entries.get(index);
        }
        return null;
    }

    public void increaseCounter() {
        index++;
    }

    public boolean isComplete() {
        return index >= entries.size();
    }

    public void reset() {
        index = 0;
    }

}

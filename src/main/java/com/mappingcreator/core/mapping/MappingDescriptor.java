package com.mappingcreator.core.mapping;

import java.util.Map;

public class MappingDescriptor {
    private int                 id;
    private String              factoryURL;
    private String              directoryName;
    private String              fullDirectoryPath;
    private String              xlsName;
    private String              xlsPath;
    private String              dictionaryName;
    private Map<String, String> asciiDictionary;
    private EdifactDictionary   edifactDictionary;

    public String getFactoryURL() {
        return factoryURL;
    }

    public void setFactoryURL(String factoryURL) {
        this.factoryURL = factoryURL;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
    }

    public String getFullDirectoryPath() {
        return fullDirectoryPath;
    }

    public void setFullDirectoryPath(String fullDirectoryPath) {
        this.fullDirectoryPath = fullDirectoryPath;
    }

    public String getXlsName() {
        return xlsName;
    }

    public void setXlsName(String xslName) {
        this.xlsName = xslName;
    }

    public String getXlsPath() {
        return xlsPath;
    }

    public void setXlsPath(String xslPath) {
        this.xlsPath = xslPath;
    }

    public Map<String, String> getAsciiDictionary() {
        return asciiDictionary;
    }

    public void setAsciiDictionary(Map<String, String> asciiDictionary) {
        this.asciiDictionary = asciiDictionary;
    }

    public EdifactDictionary getEdifactDictionary() {
        return edifactDictionary;
    }

    public void setEdifactDictionary(EdifactDictionary edifactDictionary) {
        this.edifactDictionary = edifactDictionary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDictionaryName() {
        return dictionaryName;
    }

    public void setDictionaryName(String dictionaryName) {
        this.dictionaryName = dictionaryName;
    }
}

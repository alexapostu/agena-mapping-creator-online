package com.mappingcreator.core.mapping.formula;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;

import com.mappingcreator.core.exceltransformer.ExcelFormula;
import com.mappingcreator.core.util.ExceptionUtil;
import com.mappingcreator.core.util.GlobalConfig;
import com.mappingcreator.core.util.Util;

/**
 * Init and retrieve Excel formulas
 * @author aapostu
 *
 */
public class FormulaManager {

    private static final String                          DESCRIPTOR_COMMENT_INDICATOR = "#";
    private static final String                          DESCRIPTOR_PARAM_SEPARATOR   = ";";

    private static final List<ExcelFormula>              FORMULAS                     = new ArrayList<ExcelFormula>();
    private static final Map<String, List<ExcelFormula>> FORMULAS_BY_SHEET            = new HashMap<String, List<ExcelFormula>>();
    private static final int                             SECOND_ROW_INDEX             = 1;

    public static void init() {
        String formulaDescriptorFile = GlobalConfig.get("formulaDescriptor");
        String excelTemplateFile = GlobalConfig.get("excelTemplateFile");

        initFormulaTemplates(formulaDescriptorFile);
        extractFormulasByTemplate(excelTemplateFile);
    }

    private static void extractFormulasByTemplate(String excelTemplateFile) {
        try (InputStream fis = new FileInputStream(new File(excelTemplateFile));
                HSSFWorkbook workbook = new HSSFWorkbook(fis)) {

            for (String sheetName : FORMULAS_BY_SHEET.keySet()) {
                HSSFSheet sheet = workbook.getSheet(sheetName);
                if (sheet == null) {
                    ExceptionUtil.error("Unable to locate sheet with name `" + sheetName + "` in template.");
                }
                populateSheetFormulas(sheet, FORMULAS_BY_SHEET.get(sheetName));
            }

        } catch (FileNotFoundException fnfe) {
            ExceptionUtil.error("File " + excelTemplateFile + " was not found!", fnfe);
        } catch (IOException ioexc) {
            ExceptionUtil.error("Unable to close stream for file " + excelTemplateFile, ioexc);
        }

    }

    private static void populateSheetFormulas(HSSFSheet sheet, List<ExcelFormula> sheetFormulas) {
        for (ExcelFormula formula : sheetFormulas) {
            formula.setFormula(getFormulaFromSheet(sheet, formula.getColumn()));
        }
    }

    private static String getFormulaFromSheet(HSSFSheet sheet, int column) {
        String formula = "";
        HSSFRow row = sheet.getRow(SECOND_ROW_INDEX);
        if (row != null) {
            HSSFCell cell = row.getCell(column);
            if (cell != null && cell.getCellTypeEnum().equals(CellType.FORMULA)) {
                formula = cell.getCellFormula();
            }
        }
        return formula;
    }

    private static void initFormulaTemplates(String formulaDescriptorFile) {
        try (BufferedReader br = new BufferedReader(new FileReader(formulaDescriptorFile))) {
            String line = br.readLine();

            while (line != null) {
                processDescriptorFileLine(line.trim());
                line = br.readLine();
            }
        } catch (Exception e) {
            ExceptionUtil.error("Unable to load query file!");
        }
        initHashMaps();

    }

    /**
     * Enable easy access to formulas by defining HashMaps
     */
    private static void initHashMaps() {
        for (ExcelFormula formula : FORMULAS) {
            if (!FORMULAS_BY_SHEET.containsKey(formula.getSheetName())) {
                FORMULAS_BY_SHEET.put(formula.getSheetName(), new ArrayList<ExcelFormula>());
            }

            FORMULAS_BY_SHEET.get(formula.getSheetName()).add(formula);
        }
    }

    private static void processDescriptorFileLine(String line) {
        if (!line.startsWith(DESCRIPTOR_COMMENT_INDICATOR)) {
            String[] lineContent = line.split(DESCRIPTOR_PARAM_SEPARATOR);
            if (lineContent.length < 2) {
                ExceptionUtil.error("Insuficient parameters found in descriptor file for line:" + line);
            }
            int columnNumber = Util.stringToInteger(lineContent[1], -1);
            if (columnNumber == -1) {
                ExceptionUtil.error("Unable to convert " + lineContent[1] + " to a number.");
            }
            String description = lineContent.length > 2 ? lineContent[2] : "";
            FORMULAS.add(new ExcelFormula(columnNumber, lineContent[0], description, null));
        }
    }

    public static Set<String> getFormulaSheetNames() {
        return Collections.unmodifiableSet(FORMULAS_BY_SHEET.keySet());
    }

    public static List<ExcelFormula> getFormulasBySheet(String sheetName) {
        return Collections.unmodifiableList(FORMULAS_BY_SHEET.get(sheetName));
    }
}
